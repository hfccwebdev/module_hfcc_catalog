<?php

/**
 * This file contains code specific to courses in HFC proposal-based curriculum management system.
 *
 * @see catalog_proposals.module
 */

function catalog_proposals_course_add($form, &$form_state) {
  $form = array();
  $form['existing_course'] = array(
    '#type' => 'fieldset',
    '#title' => t('Edit an Existing Course'),
    '#weight' => 0,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['existing_course']['course_selection'] = array(
    '#type' => 'select',
    '#options' => catalog_proposals_get_courses(),
  );
  $form['new_course'] = array(
    '#type' => 'fieldset',
    '#title' => t('Create a New Course'),
    '#weight' => 1,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['new_course']['subject'] = array(
    '#type' => 'select',
    '#title' => t('Course Subject'),
    '#description' => t('If a new subject code is required please contact the Registrar.'),
    '#options' => hankdata_hanksubj_options(),
  );
  $form['new_course']['number'] = array(
    '#type' => 'textfield',
    '#title' => t('Course Number'),
    '#size' => 4,
    '#maxlength' => 4,
    '#weight' => 1,
  );
  $form['new_course']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Course Title'),
    '#description' => t('This is the official title for your course and will be displayed in the catalog and on the website.'),
    '#size' => 60,
    '#maxlength' => 255,
    '#weight' => 2,
  );
  $form['course_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 100,
  );
  return $form;
}

function catalog_proposals_course_add_validate($form, &$form_state) {
  $existing = $form['existing_course']['course_selection']['#value'];
  $subject = $form['new_course']['subject']['#value'];
  $number = $form['new_course']['number']['#value'];
  $title = $form['new_course']['title']['#value'];

  if ((empty($existing . $subject . $number . $title)) || ((!empty($existing)) && (!empty($subject . $number . $title)))) {
    form_set_error('missing_or_mixed_values', 'Please select an existing course OR provide new course information');
  }
  elseif (empty($existing)) {
    if (empty($subject)) {
      form_set_error('no_subj', 'Please select a course subject.');
    }
    if (empty($number)) {
      form_set_error('no_num', 'Please provide a course number.');
    }
    elseif (strlen($number) < 3) {
      form_set_error('num_short', 'New course number is too short.');
    }
    if (empty($title)) {
      form_set_error('no_title', 'Please provide a course title.');
    }
    catalog_proposals_check_courses($subject, $number, $title);
  }
}

function catalog_proposals_course_add_submit($form, &$form_state) {
  global $user;
  $courseproposal = entity_create('node', array(
    'type' => 'course_proposal',
    'changed' => REQUEST_TIME,
    'revisions' => 1,
    'status' => 1,
    'language' => LANGUAGE_NONE,
    'uid' => $user->uid,
    'name' => $user->name,
  ));
  if (empty($form['existing_course']['course_selection']['#value'])) {
    $courseproposal->field_crs_subj['und'][0]['value'] = $form['new_course']['subject']['#value'];
    $courseproposal->field_crs_num['und'][0]['value'] = $form['new_course']['number']['#value'];
    $courseproposal->field_crs_title['und'][0]['value'] = $form['new_course']['title']['#value'];
  }
  elseif (!empty($form['existing_course']['course_selection']['#value'])) {
    $coursemaster = node_load($form['existing_course']['course_selection']['#value']);

    $suppquery = new EntityFieldQuery();
    $suppresult = $suppquery
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'supplemental_course_info')
      ->fieldCondition('field_course_master', 'target_id', $coursemaster->nid)
      ->execute();
    if(!empty($suppresult)) {
      $suppresult = reset($suppresult['node']);
      $supplemental = node_load($suppresult->nid);
    }
    else {
      $supplemental = NULL;
    }

    $courseproposal->field_crs_master_ref[LANGUAGE_NONE][]['target_id'] = $coursemaster->nid;

    catalog_proposals_load_course_prop($courseproposal, $coursemaster, $supplemental);
  }
  node_save($courseproposal);
  $form_state['redirect'] = 'node/' . $courseproposal->nid . '/edit';
}

function catalog_proposals_course_push($node) {
  if ($node->type == 'course_proposal') {
    $courseproposal = $node;

    if ($courseproposal->field_proposal_processed['und'][0]['value'] != 1) {
      if (!empty($courseproposal->field_crs_div_review)) {
        if (!empty($courseproposal->field_crs_master_ref['und'][0]['target_id'])) {
          $coursemaster = node_load($courseproposal->field_crs_master_ref['und'][0]['target_id']);
          $suppquery = new EntityFieldQuery();
          $suppresult = $suppquery
            ->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', 'supplemental_course_info')
            ->fieldCondition('field_course_master', 'target_id', $coursemaster->nid)
            ->execute();
          if(!empty($suppresult)) {
            $suppresult = reset($suppresult['node']);
            $supplemental = node_load($suppresult->nid);
          }
          else {
            $supplemental = catalog_proposals_create_course_supplemental_info();
          }
        }
        else {
          $coursemaster = catalog_proposals_create_course_master();
          $supplemental = catalog_proposals_create_course_supplemental_info();
        }

        catalog_proposals_sync_course($courseproposal, $coursemaster, $supplemental);

        $courseproposal->field_proposal_processed['und'][0]['value'] = 1;

        node_save($courseproposal);
        node_save($coursemaster);
        if (empty($supplemental->nid)) {
          $supplemental->field_course_master['und'][0]['target_id'] = $coursemaster->nid;
        }
        node_save($supplemental);

        drupal_set_message(t('Proposal successfully processed.'));
        drupal_goto('node/' . $coursemaster->nid);
      }
      else {
        drupal_set_message(t('These changes have not yet been reviewed in HANK by the division.'), 'error');
        drupal_goto('node/' . $courseproposal->nid);
      }
    }
    else {
      drupal_set_message(t('This proposal has already been approved and processed.'), 'error');
      drupal_goto('node/' . $courseproposal->nid);
    }
  }
  return array();
}

function catalog_proposals_get_courses() {
  $result = db_query("SELECT nid, title FROM node WHERE type='course_master' ORDER BY title ASC")->fetchAll();
  $options[''] = t('- None -');
  foreach ($result as $value) {
    $crspropquery = new EntityFieldQuery();
    $crspropresult = $crspropquery
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'course_proposal')
      ->fieldCondition('field_crs_master_ref', 'target_id', $value->nid)
      ->fieldCondition('field_proposal_processed', 'value', 0)
      ->execute();
    if (empty($crspropresult)) {
      $options[$value->nid] = $value->title;
    }
  }
  return $options;
}

function catalog_proposals_check_courses($subject, $number, $title) {
  $cmquery = new EntityFieldQuery();
  $propquery = new EntityFieldQuery();
  $crstitlequery = new EntityFieldQuery();
  $bundles = array(
    'course_master',
    'course_proposal',
  );
  $cmresult= $cmquery
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'course_master')
    ->fieldCondition('field_crs_subj', '#value', $subject)
    ->fieldCondition('field_crs_num', '#value', $number)
    ->execute();
  $propresult= $propquery
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'course_proposal')
    ->fieldCondition('field_crs_subj', '#value', $subject)
    ->fieldCondition('field_crs_num', '#value', $number)
    ->fieldCondition('field_proposal_processed', '#value', 0)
    ->execute();
  $crstitleresult = $crstitlequery
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', $bundles)
    ->fieldCondition('field_crs_subj', '#value', $subject)
    ->fieldCondition('field_crs_title', '#value', $title)
    ->execute();
  $hankresult = db_query("SELECT courses_id FROM hank_courses WHERE crs_subject='$subject' AND crs_no='$number'")->fetchAll();

  if (!empty($cmresult)) {
    form_set_error('course_exists', 'There is already a course master with that subject and number.');
  }
  elseif (!empty($propresult)) {
    form_set_error('proposal_exists', 'There is already an outstanding proposal for a new course with that subject and number.');
  }
  elseif (!empty($hankresult)) {
    form_set_error('hank)course_exists', 'There is already a course with that subject and number in HANK.');
  }
  elseif (!empty($crstitleresult)) {
    drupal_set_message(t('There is already a course with that title within this subject.'), 'warning');
  }
}



function catalog_proposals_load_course_prop(&$courseproposal, $coursemaster, $supplemental) {
  // Basic Info
  $courseproposal->title = $coursemaster->title;
  $courseproposal->field_crs_div = $coursemaster->field_crs_div;
  $courseproposal->field_crs_dept = $coursemaster->field_crs_dept;
  $courseproposal->field_crs_subj = $coursemaster->field_crs_subj;
  $courseproposal->field_crs_num = $coursemaster->field_crs_num;
  $courseproposal->field_crs_title = $coursemaster->field_crs_title;
  $courseproposal->field_crs_xref = $coursemaster->field_crs_xref;
  $courseproposal->field_crs_xref_id = $coursemaster->field_crs_xref_id;
  $courseproposal->field_crs_credit_hours = $coursemaster->field_crs_credit_hours;
  $courseproposal->field_crs_instructor_hours = $coursemaster->field_crs_instructor_hours;
  $courseproposal->field_crs_student_hours = $coursemaster->field_crs_student_hours;
  $courseproposal->field_crs_grading = $coursemaster->field_crs_grading;
  $courseproposal->field_crs_prq = $coursemaster->field_crs_prq;
  $courseproposal->field_crs_crq = $coursemaster->field_crs_crq;
  $courseproposal->field_crs_description = $coursemaster->field_crs_description;

  // Goals, Topics, Objectives
  $courseproposal->field_crs_goals = $coursemaster->field_crs_goals;
  $courseproposal->field_crs_topics = $coursemaster->field_crs_topics;
  $courseproposal->field_crs_lrn_objectives = $coursemaster->field_crs_lrn_objectives;
  $courseproposal->field_crs_dtl_objectives = $coursemaster->field_crs_dtl_objectives;
  $courseproposal->field_crs_general_info = $coursemaster->field_crs_general_info;

  // Assessment and Requirements
  $courseproposal->field_crs_assessment = $coursemaster->field_crs_assessment;
  $courseproposal->field_crs_requirements = $coursemaster->field_crs_requirements;
  $courseproposal->field_crs_texts = $coursemaster->field_crs_texts;

  // CPCLL
  $courseproposal->field_crs_prior_learning_options = $coursemaster->field_crs_prior_learning_options;
  $courseproposal->field_crs_cpcll_fee = $coursemaster->field_crs_cpcll_fee;
  $courseproposal->field_crs_prior_dept_exam_score = $coursemaster->field_crs_prior_dept_exam_score;
  $courseproposal->field_crs_prior_dept_exam = $coursemaster->field_crs_prior_dept_exam;
  $courseproposal->field_crs_prior_other_exam_score = $coursemaster->field_crs_prior_other_exam_score;
  $courseproposal->field_crs_prior_other_exam = $coursemaster->field_crs_prior_other_exam;
  $courseproposal->field_crs_prior_skilled_demo = $coursemaster->field_crs_prior_skilled_demo;
  $courseproposal->field_crs_prior_portfolio = $coursemaster->field_crs_prior_portfolio;
  $courseproposal->field_crs_prior_license = $coursemaster->field_crs_prior_license;
  $courseproposal->field_crs_prior_licensure = $coursemaster->field_crs_prior_licensure;
  $courseproposal->field_crs_prior_interview = $coursemaster->field_crs_prior_interview;
  $courseproposal->field_crs_prior_eval = $coursemaster->field_crs_prior_eval;

  // Supplemental
  if (!empty($supplemental)) {
    $courseproposal->field_crs_macrao = $supplemental->field_crs_macrao;
    $courseproposal->field_crs_mta = $supplemental->field_crs_mta;
    $courseproposal->field_crs_student_audit = $supplemental->field_crs_student_audit;
    $courseproposal->field_crs_transfer = $supplemental->field_crs_transfer;
    $courseproposal->field_crs_transfer_notes = $supplemental->field_crs_transfer_notes;
    $courseproposal->field_crs_waitlist = $supplemental->field_crs_waitlist;
    $courseproposal->field_program_xfer_colleges = $supplemental->field_program_xfer_colleges;
  }
}

function catalog_proposals_sync_course($courseproposal, &$coursemaster, &$supplemental) {
  // Basic Info
  $coursemaster->title = $courseproposal->title;
  $coursemaster->field_crs_div = $courseproposal->field_crs_div;
  $coursemaster->field_crs_dept = $courseproposal->field_crs_dept;
  $coursemaster->field_crs_subj = $courseproposal->field_crs_subj;
  $coursemaster->field_crs_num = $courseproposal->field_crs_num;
  $coursemaster->field_crs_title = $courseproposal->field_crs_title;
  $coursemaster->field_crs_xref = $courseproposal->field_crs_xref;
  $coursemaster->field_crs_xref_id = $courseproposal->field_crs_xref_id;
  $coursemaster->field_crs_credit_hours = $courseproposal->field_crs_credit_hours;
  $coursemaster->field_crs_instructor_hours = $courseproposal->field_crs_instructor_hours;
  $coursemaster->field_crs_student_hours = $courseproposal->field_crs_student_hours;
  $coursemaster->field_crs_grading = $courseproposal->field_crs_grading;
  $coursemaster->field_crs_prq = $courseproposal->field_crs_prq;
  $coursemaster->field_crs_crq = $courseproposal->field_crs_crq;
  $coursemaster->field_crs_description = $courseproposal->field_crs_description;

  // Goals, Topics, Objectives
  $coursemaster->field_crs_goals = $courseproposal->field_crs_goals;
  $coursemaster->field_crs_topics = $courseproposal->field_crs_topics;
  $coursemaster->field_crs_lrn_objectives = $courseproposal->field_crs_lrn_objectives;
  $coursemaster->field_crs_dtl_objectives = $courseproposal->field_crs_dtl_objectives;
  $coursemaster->field_crs_general_info = $courseproposal->field_crs_general_info;

  // Assessment and Requirements
  $coursemaster->field_crs_assessment = $courseproposal->field_crs_assessment;
  $coursemaster->field_crs_requirements = $courseproposal->field_crs_requirements;
  $coursemaster->field_crs_texts = $courseproposal->field_crs_texts;

  // CPCLL
  $coursemaster->field_crs_prior_learning_options = $courseproposal->field_crs_prior_learning_options;
  $coursemaster->field_crs_cpcll_fee = $courseproposal->field_crs_cpcll_fee;
  $coursemaster->field_crs_prior_dept_exam_score = $courseproposal->field_crs_prior_dept_exam_score;
  $coursemaster->field_crs_prior_dept_exam = $courseproposal->field_crs_prior_dept_exam;
  $coursemaster->field_crs_prior_other_exam_score = $courseproposal->field_crs_prior_other_exam_score;
  $coursemaster->field_crs_prior_other_exam = $courseproposal->field_crs_prior_other_exam;
  $coursemaster->field_crs_prior_skilled_demo = $courseproposal->field_crs_prior_skilled_demo;
  $coursemaster->field_crs_prior_portfolio = $courseproposal->field_crs_prior_portfolio;
  $coursemaster->field_crs_prior_license = $courseproposal->field_crs_prior_license;
  $coursemaster->field_crs_prior_licensure = $courseproposal->field_crs_prior_licensure;
  $coursemaster->field_crs_prior_interview = $courseproposal->field_crs_prior_interview;
  $coursemaster->field_crs_prior_eval = $courseproposal->field_crs_prior_eval;

  //Dates
  $coursemaster->field_effective_term = $courseproposal->field_effective_term;
  $coursemaster->field_crs_deactivation_date = $courseproposal->field_crs_deactivation_date;
  $coursemaster->field_crs_div_approval = $courseproposal->field_crs_div_approval;
  $coursemaster->field_crs_ac_approval = $courseproposal->field_crs_ac_approval;
  $coursemaster->field_crs_cc_approval = $courseproposal->field_crs_cc_approval;

  $coursemaster->revision = 1;
  $coursemaster->log = t('Syncronized with Course Proposal ' . $courseproposal-> nid . '.');

  // Supplemental
  $supplemental->field_crs_macrao = $courseproposal->field_crs_macrao;
  $supplemental->field_crs_mta = $courseproposal->field_crs_mta;
  $supplemental->field_crs_student_audit = $courseproposal->field_crs_student_audit;
  $supplemental->field_crs_transfer = $courseproposal->field_crs_transfer;
  $supplemental->field_crs_transfer_notes = $courseproposal->field_crs_transfer_notes;
  $supplemental->field_crs_waitlist = $courseproposal->field_crs_waitlist;
  $supplemental->field_program_xfer_colleges = $courseproposal->field_program_xfer_colleges;
  if (empty($courseproposal->field_crs_master_ref['und'][0]['target_id'])) {
    $supplemental->field_hank_courses_id = $courseproposal->field_hank_courses_id;
    $supplemental->field_crs_short_title = $courseproposal->field_crs_short_title;
    $supplemental->field_crs_gened_outcomes = $courseproposal->field_crs_gened_outcomes;
    $supplemental->field_crs_acscode = $courseproposal->field_crs_acscode;
    $supplemental->field_crs_capacity = $courseproposal->field_crs_capacity;
    $supplemental->field_crs_course_fee = $courseproposal->field_crs_course_fee;
    $supplemental->field_crs_fac_qual = $courseproposal->field_crs_fac_qual;
    $supplemental->field_crs_fee_desc = $courseproposal->field_crs_fee_desc;
    $supplemental->field_crs_lab_fee = $courseproposal->field_crs_lab_fee;
    $supplemental->field_crs_misc_dept_fee = $courseproposal->field_crs_misc_dept_fee;
    $supplemental->field_crs_reimburse_code = $courseproposal->field_crs_reimburse_code;
  }

  $supplemental->revision = 1;
  $supplemental->log = t('Syncronized with Course Proposal ' . $courseproposal-> nid . '.');
}

function catalog_proposals_create_course_master() {
  global $user;
  return entity_create('node', array(
    'type' => 'course_master',
    'changed' => REQUEST_TIME,
    'revisions' => 1,
    'status' => 1,
    'language' => LANGUAGE_NONE,
    'uid' => $user->uid,
    'name' => $user->name,
  ));
}

function catalog_proposals_create_course_supplemental_info() {
  global $user;
  return entity_create('node', array(
    'type' => 'supplemental_course_info',
    'changed' => REQUEST_TIME,
    'revisions' => 1,
    'status' => 1,
    'language' => LANGUAGE_NONE,
    'uid' => $user->uid,
    'name' => $user->name,
  ));
}
