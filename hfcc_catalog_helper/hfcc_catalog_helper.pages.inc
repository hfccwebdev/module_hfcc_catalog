<?php

/**
 * This file contains the main page callbacks for the HFCC Catalog Helper module.
 *
 * @see hfcc_catalog_helper.module
 * @see hfcc_catalog.module
 */

/**
 * Main page callback for catalog helper tools.
 */
function hfcc_catalog_helper_page($node) {
  $output = array();
  switch ($node->type) {
    case 'course_master':
      return drupal_get_form('hfcc_catalog_helper_cmform', $node);
      break;
    case 'catalog_course':
      return drupal_get_form('hfcc_catalog_helper_ccform', $node);
      break;
  }
  return $output;
}

/**
 * Generate helper action form for Course Master nodes.
 */
function hfcc_catalog_helper_cmform($form, $form_state, $course_master) {
  $form = array();

  $nid = $course_master->nid;
  $subject = $course_master->field_crs_subj['und'][0]['value'];
  $number = $course_master->field_crs_num['und'][0]['value'];
  $found = FALSE;
  if ($catalog_course = hfcc_catalog_helper_get_cc_by_reference($nid)) {
    $found = 'reference';
  }
  elseif ($catalog_course = hfcc_catalog_helper_get_cc_by_number($subject, $number)) {
    drupal_set_message(t('Found Catalog entry by course number. Synchronize to fix entity reference.'), 'warning');
    $found = 'number';
  }
  if ($found) {
    hfcc_catalog_helper_compare($form, $course_master, $catalog_course, FALSE);
  }
  else {
    // Create a new (unsaved) node to display in the right panel.
    $catalog_course = hfcc_catalog_helper_create_catalog_course();
    hfcc_catalog_helper_sync_fields($course_master, $catalog_course);
    hfcc_catalog_helper_compare($form, $course_master, $catalog_course, TRUE);
  }
  return $form;
}

/**
 * Generate helper action form for Catalog Course nodes.
 */
function hfcc_catalog_helper_ccform($form, $form_state, $catalog_course) {
  $form = array();

  $subject = $catalog_course->field_crs_subj['und'][0]['value'];
  $number = $catalog_course->field_crs_num['und'][0]['value'];
  $found = FALSE;

  if (!empty($catalog_course->field_course_master)) {
    $course_master = node_load($catalog_course->field_course_master['und'][0]['target_id']);
    $found = 'reference';
  }
  elseif ($course_master = hfcc_catalog_helper_get_cm_by_number($subject, $number)) {
    drupal_set_message(t('Found Catalog entry by course number. Synchronize to fix entity reference.'), 'warning');
    $found = 'number';
  }
  if ($found) {
    hfcc_catalog_helper_compare($form, $course_master, $catalog_course, FALSE);
  }
  else {
    drupal_set_message(t('Course Master not found.'), 'warning');
    $form[] = array('#markup' => t('Could not find a Course Master matching this Course Subject and Number.'));
  }
  return $form;
}

/**
 * Build fieldset for comparing Course Masters to Course Catalog.
 *
 * @todo get rid of this mess and use an Entity View Mode instead.
 */
function hfcc_catalog_helper_compare(&$form, $course_master, $catalog_course, $is_new) {
  // if (function_exists('dpm')) {
  //   dpm($course_master, 'course_master');
  //   dpm($catalog_course, 'catalog_course');
  // }

  $cm_node = clone $course_master;
  $cm_elements = node_view($cm_node, 'catalog_helper');
  $cc_node = clone $catalog_course;
  if (empty($cc_node->nid)) {
    $cc_node->nid = 0;
    $cc_node->created = time();
    $cc_node->promote = 0;
    $cc_node->status = 0;
    $cc_node->sticky = 0;
  }
  $cc_elements = node_view($cc_node, 'catalog_helper');

  // Unset #theme so node theming won't be applied.
  // We only want the fields, not a completely themed node.

  unset($cm_elements['#theme']);
  unset($cc_elements['#theme']);

  // Build Course Master Fieldset

  $form['course_master'] = array(
    '#type' => 'fieldset',
    '#title' => t('Course Master'),
    $cm_elements,
  );
  $form['course_master']['course_master_title'] = array(
    '#type' => 'item',
    '#field_name' => 'hfcc_course_master_title',
    '#label' => t('Title'),
    '#label_display' => 'inline',
    '#markup' => l($course_master->title, 'node/' . $course_master->nid),
    '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    '#weight' => -20,
  );
  $form['course_master']['course_master_current_revision'] = array(
    '#type' => 'item',
    '#field_name' => 'hfcc_course_master_current_revision',
    '#label' => t('Current Revision'),
    '#label_display' => 'inline',
    '#markup' => !empty($course_master->workbench_moderation['current']) ? hfcc_catalog_helper_revision_summary($course_master->workbench_moderation['current']): '',
    '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    '#weight' => -19,
  );
  $form['course_master']['course_master_published_revision'] = array(
    '#type' => 'item',
    '#field_name' => 'hfcc_course_master_current_revision',
    '#label' => t('Published Revision'),
    '#label_display' => 'inline',
    '#markup' => !empty($course_master->workbench_moderation['published']) ? hfcc_catalog_helper_revision_summary($course_master->workbench_moderation['published']): '',
    '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    '#weight' => -18,
  );

  // Build Catalog Course Fieldset

  $form['catalog_course'] = array(
    '#type' => 'fieldset',
    '#title' => t('Catalog Course'),
    $cc_elements,
  );

  $form['catalog_course']['catalog_course_title'] = array(
    '#type' => 'item',
    '#field_name' => 'hfcc_catalog_course_title',
    '#label' => t('Title'),
    '#label_display' => 'inline',
    '#markup' => !empty($catalog_course->nid) ? l($catalog_course->title, 'node/' . $catalog_course->nid) : $catalog_course->title,
    '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    '#weight' => -20,
  );
  $form['catalog_course']['catalog_course_current_state'] = array(
    '#type' => 'item',
    '#field_name' => 'hfcc_catalog_course_current_state',
    '#label' => t('Last Changed'),
    '#label_display' => 'inline',
    '#markup' => !empty($catalog_course->changed) ? format_date($catalog_course->changed, 'custom', 'n/j/Y g:ia') : '',
    '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    '#weight' => -19,
  );

  // Pass the node objects so we can process them on return.

  $form['#course_master'] = array(
    '#type' => 'value',
    '#value' => $course_master,
  );
  $form['#catalog_course'] = array(
    '#type' => 'value',
    '#value' => $catalog_course,
  );

  $form['is_new'] = array(
    '#type' => 'value',
    '#value' => $is_new,
  );

  if ($is_new) {
    $form['create'] = array(
      '#type' => 'submit',
      '#value' => t('Create new Catalog Entry'),
      '#description' => t('Create new Catalog Entry from Course Master'),
      '#weight' => 15,
      '#submit' => array('hfcc_catalog_helper_create_submit'),
    );
  }
  else {
    $form['sync'] = array(
      '#type' => 'submit',
      '#value' => t('Synchronize Now'),
      '#description' => t('Update Catalog Entry to match information from Course Master'),
      '#weight' => 15,
      '#submit' => array('hfcc_catalog_helper_sync_submit'),
    );
  }

}

/**
 * Form submission handler to create Catalog Course entry.
 */
function hfcc_catalog_helper_create_submit($form, &$form_state) {
  $catalog_course = $form['#catalog_course']['#value'];
  $catalog_course->revision = 1;
  $catalog_course->log = t('Created from Course Master using HFC Catalog Helper.');
  $catalog_course->status = 1;
  node_save($catalog_course);
  if (empty($catalog_course->nid)) {
    drupal_set_message(t('Error creating Catalog Entry!'), 'error');
  }
}

/**
 * Form submission handler to synchronize Catalog Course entry.
 */
function hfcc_catalog_helper_sync_submit($form, &$form_state) {
  $course_master = $form['#course_master']['#value'];
  $catalog_course = $form['#catalog_course']['#value'];

  hfcc_catalog_helper_sync_fields($course_master, $catalog_course);

  $catalog_course->log = t('Synchronized with Course Master using HFC Catalog Helper.');
  node_save($catalog_course);
}

/**
 * Display comparison of Course Master and HANK data.
 */
function hfcc_catalog_helper_hank_form($form, $form_state, $course_master) {
  $cm_node = clone $course_master;
  $cm_elements = node_view($cm_node, 'catalog_helper');
  unset($cm_elements['#theme']);

  $form['course_master'] = array(
    '#type' => 'fieldset',
    '#title' => t('Course Master'),
    $cm_elements,
  );
  $form['course_master']['course_master_title'] = array(
    '#type' => 'item',
    '#field_name' => 'hfcc_course_master_title',
    '#label' => t('Title'),
    '#label_display' => 'inline',
    '#markup' => l($course_master->title, 'node/' . $course_master->nid),
    '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    '#weight' => -20,
  );
  $form['course_master']['course_master_current_revision'] = array(
    '#type' => 'item',
    '#field_name' => 'hfcc_course_master_current_revision',
    '#label' => t('Current Revision'),
    '#label_display' => 'inline',
    '#markup' => !empty($course_master->workbench_moderation['current']) ? hfcc_catalog_helper_revision_summary($course_master->workbench_moderation['current']): '',
    '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    '#weight' => -19,
  );
  $form['course_master']['course_master_published_revision'] = array(
    '#type' => 'item',
    '#field_name' => 'hfcc_course_master_current_revision',
    '#label' => t('Published Revision'),
    '#label_display' => 'inline',
    '#markup' => !empty($course_master->workbench_moderation['published']) ? hfcc_catalog_helper_revision_summary($course_master->workbench_moderation['published']): '',
    '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    '#weight' => -18,
  );

  $form['hank_course'] = array(
    '#type' => 'fieldset',
    '#title' => t('HANK Course'),
  );

  $args = array(
    ':subj' => $course_master->field_crs_subj['und'][0]['value'],
    ':num' => $course_master->field_crs_num['und'][0]['value'],
  );
  $hank_course = db_query("SELECT * FROM {hank_courses} WHERE crs_subject=:subj AND crs_no=:num", $args)->fetchAll();
  $hank_course = !empty($hank_course) ? reset($hank_course) : NULL;
  if (!empty($hank_course)) {
    // dpm ($hank_course, 'current_courses');

    $form['hank_course']['hank_course_title'] = array(
      '#type' => 'item',
      '#field_name' => 'hfcc_catalog_course_title',
      '#label' => t('Title'),
      '#label_display' => 'inline',
      '#markup' => t('!name: !title', array('!name' => $hank_course->crs_name, '!title' => $hank_course->crs_title)),
      '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    );
    // $form['hank_course']['hank_course_div'] = array(
    //   '#type' => 'item',
    //   '#field_name' => 'hfcc_course_master_div',
    //   '#label' => t('Division'),
    //   '#label_display' => 'inline',
    //   '#markup' => !empty($hank_course->field_crs_div) ? check_plain($hank_course->field_crs_div) : '',
    //   '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    // );
    // $form['hank_course']['hank_course_dept'] = array(
    //   '#type' => 'item',
    //   '#field_name' => 'hfcc_course_master_dept',
    //   '#label' => t('Department'),
    //   '#label_display' => 'inline',
    //   '#markup' => !empty($hank_course->field_crs_dept) ? check_plain($hank_course->field_crs_dept) : '',
    //   '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    // );
    $form['hank_course']['hank_course_subj'] = array(
      '#type' => 'item',
      '#field_name' => 'hfcc_course_master_subj',
      '#label' => t('Course Subject'),
      '#label_display' => 'inline',
      '#markup' => check_plain($hank_course->crs_subject),
      '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    );
    $form['hank_course']['hank_course_num'] = array(
      '#type' => 'item',
      '#field_name' => 'hfcc_course_master_num',
      '#label' => t('Course Number'),
      '#label_display' => 'inline',
      '#markup' => check_plain($hank_course->crs_no),
      '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    );
    $form['hank_course']['hank_course_short_title'] = array(
      '#type' => 'item',
      '#field_name' => 'hfcc_course_master_short_title',
      '#label' => t('Short Title'),
      '#label_display' => 'inline',
      '#markup' => check_plain($hank_course->crs_title),
      '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    );
    $form['hank_course']['hank_course_credit_hours'] = array(
      '#type' => 'item',
      '#field_name' => 'hfcc_course_master_credit_hours',
      '#label' => t('Credit Hours'),
      '#label_display' => 'inline',
      '#markup' => check_plain($hank_course->crs_min_cred),
      '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    );
    // $form['hank_course']['hank_course_prq'] = array(
    //   '#type' => 'item',
    //   '#field_name' => 'hfcc_course_master_prq',
    //   '#label' => t('Prerequisites'),
    //   '#label_display' => 'inline',
    //   '#markup' => !empty($hank_course->field_crs_prq) ? check_plain($hank_course->field_crs_prq) : '',
    //   '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    // );
    // $form['hank_course']['hank_course_crq'] = array(
    //   '#type' => 'item',
    //   '#field_name' => 'hfcc_course_master_crq',
    //   '#label' => t('Co-requisites'),
    //   '#label_display' => 'inline',
    //   '#markup' => !empty($hank_course->field_crs_crq) ? check_plain($hank_course->field_crs_crq) : '',
    //   '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    // );
    // $form['hank_course']['hank_course_description'] = array(
    //   '#type' => 'item',
    //   '#field_name' => 'hfcc_course_master_description',
    //   '#label' => t('Description'),
    //   '#label_display' => 'above',
    //   '#markup' => check_markup($hank_course->field_crs_description),
    //   '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    // );
    $form['hank_course']['hank_course_last_term'] = array(
      '#type' => 'item',
      '#field_name' => 'hfcc_course_master_last_term',
      '#label' => t('Last Term'),
      '#label_display' => 'inline',
      '#markup' => !empty($hank_course->crs_last_term) ? check_plain($hank_course->crs_last_term) : NULL,
      '#theme_wrappers' => array('hfcc_global_pseudo_field'),
    );

  }
  else {
    $form['hank_courses']['message'] = array(
      '#prefix' => '<strong>',
      '#markup' => t('No matching HANK course found.'),
      '#suffix' => '</strong>',
    );
  }
  return $form;
}

