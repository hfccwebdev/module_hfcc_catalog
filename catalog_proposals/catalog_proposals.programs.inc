<?php

/**
 * This file contains code specific to programs in HFC proposal-based curriculum management system.
 *
 * @see catalog_proposals.module
 */

function catalog_proposals_program_add($form, &$form_state) {
  $form = array();
  $form['new_program'] = array(
    '#type' => 'fieldset',
    '#title' => t('Create a New Program'),
    '#weight' => 0,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['new_program']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#size' => 60,
    '#maxlength' => 255,
    '#weight' => 1,
  );
  $form['new_program']['degree_type'] = array(
    '#type' => 'select',
    '#title' => t('Degree Type'),
    '#options' => catalog_proposals_get_degree_types(),
    '#weight' => 2,
  );
  $form['existing_program'] = array(
    '#type' => 'fieldset',
    '#title' => t('Edit an Existing Program'),
    '#weight' => 1,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['existing_program']['program_selection'] = array(
    '#type' => 'select',
    '#options' => catalog_proposals_get_programs(),
  );
  $form['program_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 100,
  );
  return $form;
}

function catalog_proposals_program_add_validate($form, &$form_state) {
  $existing = $form['existing_program']['program_selection']['#value'];
  $title = $form['new_program']['title']['#value'];
  $type = $form['new_program']['degree_type']['#value'];

  if ((empty($existing . $title . $type)) || ((!empty($existing)) && (!empty($title . $type)))) {
    form_set_error('missing_or_mixed_values', 'Please select an existing program OR provide new program information');
  }
  elseif (empty($existing)) {
    if ((!empty($title)) && (empty($type))) {
    form_set_error('no_deg_type', 'Please select a degree type for this new program.');
    }
    elseif ((empty($title)) && (!empty($type))) {
      form_set_error('no_title', 'Please provide a title for this new program.');
    }
    else {
      catalog_proposals_check_programs($title, $type);
    }
  }
}

function catalog_proposals_program_add_submit($form, &$form_state) {
  global $user;
  $programproposal = entity_create('node', array(
    'type' => 'program_proposal',
    'changed' => REQUEST_TIME,
    'revisions' => 1,
    'status' => 1,
    'language' => LANGUAGE_NONE,
    'uid' => $user->uid,
    'name' => $user->name,
  ));
  if (empty($form['existing_program']['program_selection']['#value'])) {
    $programproposal->field_program_title['und'][0]['value'] = $form['new_program']['title']['#value'];
    $programproposal->field_program_type['und'][0]['value'] = $form['new_program']['degree_type']['#value'];
  }
  elseif (!empty($form['existing_program']['program_selection']['#value'])) {
    $programmaster = node_load($form['existing_program']['program_selection']['#value']);

    $suppquery = new EntityFieldQuery();
    $suppresult = $suppquery
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'supplemental_program_info')
      ->fieldCondition('field_program', 'target_id', $programmaster->nid)
      ->execute();
    if(!empty($suppresult)) {
      $suppresult = reset($suppresult['node']);
      $supplemental = node_load($suppresult->nid);
    }
    else {
      $supplemental = NULL;
    }

    $programproposal->field_program_master_ref[LANGUAGE_NONE][]['target_id'] = $programmaster->nid;

    catalog_proposals_load_program_prop($programproposal, $programmaster, $supplemental);
  }
  node_save($programproposal);
  $form_state['redirect'] = 'node/' . $programproposal->nid . '/edit';
}

function catalog_proposals_get_degree_types() {
  // This is probably required to prevent every new program from being an Area of Study
  $options = array(
    '' => '- Select -',
  );
  $types = hfcc_catalog_degree_types();
  $options = array_merge($options, $types);
  return $options;
}

function catalog_proposals_get_programs() {
  $result = db_query("SELECT nid, title FROM node WHERE type='program_master' ORDER BY title ASC")->fetchAll();
  $options[''] = t('- None -');
  foreach ($result as $value) {
    $progpropquery = new EntityFieldQuery();
    $progpropresult = $progpropquery
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'program_proposal')
      ->fieldCondition('field_program_master_ref', 'target_id', $value->nid)
      ->execute();
    if (empty($progpropresult)) {
      $options[$value->nid] = $value->title;
    }
  }
  return $options;
}

function catalog_proposals_check_programs($title, $type) {
  $progtitlequery = new EntityFieldQuery();
  $progpropquery = new EntityFieldQuery();

  $progtitleresult = $progtitlequery
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle','program_master')
    ->propertyCondition('title', $title)
    ->fieldCondition('field_program_type', '#value', $type)
    ->execute();
  $progpropresult = $progpropquery
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle','program_proposal')
    ->fieldCondition('field_program_title', '#value', $title)
    ->fieldCondition('field_program_type', '#value', $type)
    ->execute();
  if (!empty($progtitleresult)) {
    form_set_error('program_exists', 'There is already a program of this type with this title.');
  }
  elseif (!empty($progpropresult)) {
    form_set_error('proposal_exists', 'There is already an outstanding proposal for a new program of this type with this title.');
  }
}

function catalog_proposals_load_program_prop(&$programproposal, $programmaster, $supplemental) {
  // Program Info
  $programproposal->field_program_title[LANGUAGE_NONE][0]['value'] = $programmaster->title;
  $programproposal->field_program_status = $programmaster->field_program_status;
  $programproposal->field_program_type = $programmaster->field_program_type;
  $programproposal->field_program_division = $programmaster->field_program_division;
  $programproposal->field_program_department = $programmaster->field_program_department;
  $programproposal->field_program_office_contact = $programmaster->field_program_office_contact;
  $programproposal->field_program_contacts = $programmaster->field_program_contacts;
  $programproposal->field_program_year = $programmaster->field_program_year;

  //Program Description
  $programproposal->field_program_description = $programmaster->field_program_description;
  $programproposal->field_program_learning_outcomes = $programmaster->field_program_learning_outcomes;
  $programproposal->field_program_primary_skills = $programmaster->field_program_primary_skills;
  $programproposal->field_program_career_opp = $programmaster->field_program_career_opp;
  $programproposal->field_program_xfer_colleges = $programmaster->field_program_xfer_colleges;
  $programproposal->field_program_exposure = $programmaster->field_program_exposure;
  $programproposal->field_program_licensure = $programmaster->field_program_licensure;
  $programproposal->field_program_accreditation = $programmaster->field_program_accreditation;
  $programproposal->field_program_expected_duration = $programmaster->field_program_expected_duration;
  $programproposal->field_program_completion_limits = $programmaster->field_program_completion_limits;

  // General Education
  $programproposal->field_program_gened_credits = $programmaster->field_program_gened_credits;
  $programproposal->field_program_gened_civil_req = $programmaster->field_program_gened_civil_req;
  $programproposal->field_program_gened_civil = $programmaster->field_program_gened_civil;
  $programproposal->field_program_gened_comm_req = $programmaster->field_program_gened_comm_req;
  $programproposal->field_program_gened_comm = $programmaster->field_program_gened_comm;
  $programproposal->field_program_gened_comp_req = $programmaster->field_program_gened_comp_req;
  $programproposal->field_program_gened_comp = $programmaster->field_program_gened_comp;
  $programproposal->field_program_gened_crit_req = $programmaster->field_program_gened_crit_req;
  $programproposal->field_program_gened_crit = $programmaster->field_program_gened_crit;
  $programproposal->field_program_gened_quant_req = $programmaster->field_program_gened_quant_req;
  $programproposal->field_program_gened_quant = $programmaster->field_program_gened_quant;
  $programproposal->field_program_gened_hum_req = $programmaster->field_program_gened_hum_req;
  $programproposal->field_program_gened_hum = $programmaster->field_program_gened_hum;
  $programproposal->field_program_gened_science_req = $programmaster->field_program_gened_science_req;
  $programproposal->field_program_gened_science = $programmaster->field_program_gened_science;
  $programproposal->field_program_gened_misc_req = $programmaster->field_program_gened_misc_req;
  $programproposal->field_program_additional = $programmaster->field_program_additional;
  $programproposal->field_program_gened_misc = $programmaster->field_program_gened_misc;

  // Degree-specific Requirements
  $programproposal->field_program_degsp_hours = $programmaster->field_program_degsp_hours;
  $programproposal->field_program_degsp = $programmaster->field_program_degsp;

  // Courses
  $programproposal->field_program_admission = $programmaster->field_program_admission;
  $programproposal->field_program_admission_req = $programmaster->field_program_admission_req;
  $programproposal->field_program_rqcor_title = $programmaster->field_program_rqcor_title;
  $programproposal->field_program_rqcor_list = $programmaster->field_program_rqcor_list;
  $programproposal->field_program_rqcor_hours = $programmaster->field_program_rqcor_hours;
  $programproposal->field_program_rqcor_note = $programmaster->field_program_rqcor_note;
  $programproposal->field_program_rqsup_title = $programmaster->field_program_rqsup_title;
  $programproposal->field_program_rqsup_list = $programmaster->field_program_rqsup_list;
  $programproposal->field_program_rqsup_hours = $programmaster->field_program_rqsup_hours;
  $programproposal->field_program_rqsup_note = $programmaster->field_program_rqsup_note;
  $programproposal->field_program_elect_title = $programmaster->field_program_elect_title;
  $programproposal->field_program_elect_list = $programmaster->field_program_elect_list;
  $programproposal->field_program_elect_hours = $programmaster->field_program_elect_hours;
  $programproposal->field_program_elect_note = $programmaster->field_program_elect_note;
  $programproposal->field_program_credit_hours = $programmaster->field_program_credit_hours;
}
