<?php

/**
 * This file contains operational code for the HFCC Catalog Helper module.
 *
 * @see hfcc_catalog_helper.module
 * @see hfcc_catalog.module
 */

/**
 * Returns a list of programs that use the currently displayed Course Master.
 */
function hfcc_catalog_helper_course_usage() {
  $output = array();
  $id = NULL;
  if ($node = menu_get_object()) {
    if ($node->type == 'catalog_course') {
      $id = $node->nid;
    }
    elseif ($node->type == 'course_master') {
      if ($cc = hfcc_catalog_helper_get_cc_by_reference($node->nid)) {
        $id = $cc->nid;
      }
    }
  }
  if ($id) {

    // Add Required Core Courses
    $rows = array();
    $query = "SELECT l.entity_id AS nid, n.title, t.field_program_type_value AS type
      FROM {field_data_field_program_rqcor_list} l
      LEFT JOIN {node} n ON l.entity_id = n.nid AND l.revision_id = n.vid
      LEFT JOIN {field_data_field_program_type} t on l.entity_id = t.entity_id AND l.revision_id = t.revision_id
      WHERE field_program_rqcor_list_course_id = :id";
    $result = db_query($query, array(':id' => $id))->fetchAll();
    foreach ($result as $item) {
      $title = $item->title;
      $title .= !empty($item->type) ? ' (' . $item->type . ')' : '';
      $rows[] = l($title, 'node/' .  $item->nid);
    }
    if (!empty($rows)) {
      $output[] = array(
        '#prefix' => '<h3>',
        '#markup' => t('Required Core Courses'),
        '#suffix' => '</h3>',
      );
      $output[] = array(
        '#theme' => 'item_list',
        '#items' => $rows,
      );
    }

    // Add Required Support Courses
    $rows = array();
    $query = "SELECT l.entity_id AS nid, n.title, t.field_program_type_value AS type
      FROM {field_data_field_program_rqsup_list} l
      LEFT JOIN {node} n ON l.entity_id = n.nid AND l.revision_id = n.vid
      LEFT JOIN {field_data_field_program_type} t on l.entity_id = t.entity_id AND l.revision_id = t.revision_id
      WHERE field_program_rqsup_list_course_id = :id";
    $result = db_query($query, array(':id' => $id))->fetchAll();
    foreach ($result as $item) {
      $title = $item->title;
      $title .= !empty($item->type) ? ' (' . $item->type . ')' : '';
      $rows[] = l($title, 'node/' .  $item->nid);
    }
    if (!empty($rows)) {
      $output[] = array(
        '#prefix' => '<h3>',
        '#markup' => t('Required Support Courses'),
        '#suffix' => '</h3>',
      );
      $output[] = array(
        '#theme' => 'item_list',
        '#items' => $rows,
      );
    }

    // Add Elective Courses
    $rows = array();
    $query = "SELECT l.entity_id AS nid, n.title FROM {field_data_field_program_elect_list} l
      LEFT JOIN {node} n ON l.entity_id = n.nid
      WHERE field_program_elect_list_course_id = :id";
    $result = db_query($query, array(':id' => $id))->fetchAll();
    foreach ($result as $item) {
      $rows[] = l($item->title, 'node/' .  $item->nid);
    }
    if (!empty($rows)) {
      $output[] = array(
        '#prefix' => '<h3>',
        '#markup' => t('Elective Courses'),
        '#suffix' => '</h3>',
      );
      $output[] = array(
        '#theme' => 'item_list',
        '#items' => $rows,
      );
    }

  }
  return !empty($output) ? $output : NULL;
}
