<?php

/**
 * This file contains the Supplemental Info comparison page callbacks for the HFCC Catalog Helper module.
 *
 * @see hfcc_catalog_helper.module
 * @see hfcc_catalog.module
 */

/**
 * Main page callback for Supplemental Info comparison tools.
 */
function hfcc_catalog_helper_supplemental_compare_page() {
  drupal_set_title(t('Supplemental Info Comparison'), PASS_THROUGH);

  $result = db_query("SELECT nid FROM {node} WHERE type='new_course_application' ORDER BY 'title'")->fetchAll();

  $rows = array();

  foreach ($result as $item) {
    $application = node_load($item->nid);

    // Create an entity_metadata_wrapper() for this node.
    // @see https://www.drupal.org/documentation/entity-metadata-wrappers

    $appwrapper = entity_metadata_wrapper('node', $application);

    $course_master_nid = $appwrapper->field_course_master->value()->nid;

    $app_array = array(
      'acs' => $appwrapper->field_crs_acscode->value(),
      'crs_fee' => $appwrapper->field_crs_course_fee->value(),
      'lab_fee' => $appwrapper->field_crs_lab_fee->value(),
      'misc_fee' => $appwrapper->field_crs_misc_dept_fee->value(),
      'capacity' => $appwrapper->field_crs_capacity->value(),
      'waitlist' => $appwrapper->field_crs_waitlist->value(),
    );

    $query = new EntityFieldQuery();
    $supp_result = $query
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'supplemental_course_info')
      ->fieldCondition('field_course_master', 'target_id', $course_master_nid)
      ->execute();

    if (!empty($supp_result['node'])) {
      $supp_result = reset($supp_result['node']);
      $supplement = node_load($supp_result->nid);
      $suppwrapper = entity_metadata_wrapper('node', $supplement);

      $supp_array = array(
        'acs' => $suppwrapper->field_crs_acscode->value(),
        'crs_fee' => $suppwrapper->field_crs_course_fee->value(),
        'lab_fee' => $suppwrapper->field_crs_lab_fee->value(),
        'misc_fee' => $suppwrapper->field_crs_misc_dept_fee->value(),
        'capacity' => $suppwrapper->field_crs_capacity->value(),
        'waitlist' => $suppwrapper->field_crs_waitlist->value(),
      );

      // Translate Waitlist Booleans to Y/N
      if ($app_array['waitlist'] == 1) {
        $app_array['waitlist'] = t('Y');
      }
      else {
        $app_array['waitlist'] = t('N');
      }

      if ($supp_array['waitlist'] == 1) {
        $supp_array['waitlist'] = t('Y');
      }
      else {
        $supp_array['waitlist'] = t('N');
      }

      // Mark empty fields as 'Missing'
      foreach ($app_array as &$app_item) {
        if (empty($app_item)) {
          $app_item = t('---');
        }
      }

      foreach ($supp_array as &$supp_item) {
        if (empty($supp_item)) {
          $supp_item = t('---');
        }
      }

      // If the NCA doesn't match the Supplemental Info, display it
      if ($app_array <> $supp_array) {
        $rows[] = array(
          t(l(check_plain($appwrapper->field_course_master->value()->title), 'node/' . $item->nid)),
          t(l('View Supplement', 'node/' . $supp_result->nid)),
          $app_array['acs'] . " / " . $supp_array['acs'],
          $app_array['crs_fee'] . " / " . $supp_array['crs_fee'],
          $app_array['lab_fee'] . " / " . $supp_array['lab_fee'],
          $app_array['misc_fee'] . " / " . $supp_array['misc_fee'],
          $app_array['capacity'] . " / " . $supp_array['capacity'],
          $app_array['waitlist'] . " / " . $supp_array['waitlist'],
        );
      }
    }
    else {
      $rows[] = array(
        t(l(check_plain($appwrapper->field_course_master->value()->title), 'node/' . $item->nid)),
        array(
          'colspan' => '7',
          'data' => t('No Supplemental Info Found!'),
        ),
      );
    }
  }

  return array(
    '#theme' => 'table',
    '#header' => array(
      t('Application'),
      t('Supplement'),
      t('ACS Code'),
      t('Course Fee'),
      t('Lab Fee'),
      t('Misc Fee'),
      t('Capacity'),
      t('Waitlist'),
    ),
    '#rows' => $rows,
    '#empty' => t('No mismatches found.'),
  );
}

/**
 * Page callback for NCAs without Supplemental Info
 */
function hfcc_catalog_helper_supplemental_missing_page() {
  drupal_set_title(t('Supplemental Info Missing'), PASS_THROUGH);

  $result = db_query("SELECT nid FROM {node} WHERE type='new_course_application' ORDER BY 'title'")->fetchAll();

  $rows = array();

  foreach ($result as $item) {
    $application = node_load($item->nid);

    // Create an entity_metadata_wrapper() for this node.
    // @see https://www.drupal.org/documentation/entity-metadata-wrappers

    $appwrapper = entity_metadata_wrapper('node', $application);

    $course_master_nid = $appwrapper->field_course_master->value()->nid;

    $query = new EntityFieldQuery();
    $supp_result = $query
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'supplemental_course_info')
      ->fieldCondition('field_course_master', 'target_id', $course_master_nid)
      ->execute();

    if (empty($supp_result['node'])) {
      $link = l('Create one?', 'new-course-applications/supplemental/add/' . $item->nid);
      $rows[] = array(
        t(l(check_plain($appwrapper->field_course_master->value()->title), 'node/' . $item->nid)),
        t('No Supplemental Info Found! !link', array('!link' => $link)),
      );
    }
  }

  return array(
    '#theme' => 'table',
    '#header' => array(
      t('Application'),
      t('Supplement'),
    ),
    '#rows' => $rows,
    '#empty' => t('No new course applications without supplemental info found.'),
  );
}

/**
 * Create a new Supplemental Course Info node from a New Course Application if one doesn't already exist.
 */
function hfcc_catalog_helper_create_supplement($node) {
  if($node->type =='new_course_application') {
    $application = $node;

    $query = new EntityFieldQuery();
    $supp_result = $query
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'supplemental_course_info')
      ->fieldCondition('field_course_master', 'target_id', $application->field_course_master['und'][0]['target_id'])
      ->execute();

    if (empty($supp_result['node'])) {
      global $user;
      $supplement = entity_create('node', array(
        'type' => 'supplemental_course_info',
        'changed' => REQUEST_TIME,
        'revisions' => 1,
        'status' => 1,
        'language' => LANGUAGE_NONE,
        'uid' => $user->uid,
        'name' => $user->name,
      ));

      $supplement->field_course_master['und'][0]['target_id'] = $application->field_course_master['und'][0]['target_id'];
      $supplement->field_crs_acscode = $application->field_crs_acscode;
      $supplement->field_crs_course_fee = $application->field_crs_course_fee;
      $supplement->field_crs_lab_fee = $application->field_crs_lab_fee;
      $supplement->field_crs_misc_dept_fee = $application->field_crs_misc_dept_fee;
      $supplement->field_crs_capacity = $application->field_crs_capacity;
      $supplement->field_crs_waitlist = $application->field_crs_waitlist;
      $supplement->revision = 1;

      node_save($supplement);

      if (!empty($supplement->nid)) {
        drupal_goto('node/' . $supplement->nid);
      }
      else {
        drupal_set_message(t('Error creating Supplemental Course Info.'), 'error');
        drupal_goto('new-course-applications/supplemental-missing');
      }
    }
    else {
      drupal_set_message(t('Supplemental information already exists for this New Course Application.'), 'error');
      drupal_goto('new-course-applications/supplemental-missing');
    }
  }
  else {
    drupal_set_message(t("Hey, this isn't a New Course Application!"), 'error');
    drupal_goto('new-course-applications/supplemental-missing');
  }
  return array();
}
