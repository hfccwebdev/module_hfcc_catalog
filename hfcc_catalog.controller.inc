<?php

/**
 * @file
 * Interface and Controller for HFCC Catalog entities.
 */

/**
 * StaffDir class.
 */
class XferCollegeEntity extends Entity {
  protected function defaultLabel() {
    return check_plain($this->name);
  }
}
