<?php

/**
 * @file
 * Views handlers for catalog_proposals.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function catalog_proposals_views_data_alter(&$data) {
  $data['field_data_field_crs_master_ref']['field_new_course'] = array(
    'group' => t('Content'),
    'title' => t('New Course'),
    'help' => t('Display "New Course" if the proposal is for a new course.'),
    'real field' => 'field_crs_master_ref_target_id',
    'field' => array(
      'handler' => 'catalog_proposals_handler_field_new_course',
      'click sortable' => FALSE,
    ),
  );
  $data['node']['actions'] = array(
    'group' => t('Content'),
    'title' => t('Course Proposal Actions'),
    'help' => t('Provide appropriate course proposal actions.'),
    'real field' => 'nid',
    'field' => array(
      'handler' => 'catalog_proposals_handler_course_actions',
      'click sortable' => FALSE,
    ),
  );
  $data['node']['new_course'] = array(
    'group' => t('Content'),
    'title' => t('New Course Admin'),
    'help' => t('Provide status of new course administrative details.'),
    'real field' => 'nid',
    'field' => array(
      'handler' => 'catalog_proposals_handler_new_course_admin',
      'click sortable' => FALSE,
    ),
  );
}

/**
 * Return a string if the proposal is not referencing an existing course.
 *
 * @ingroup views_field_handlers
 */
class catalog_proposals_handler_field_new_course extends views_handler_field {
  public function render($values) {
    $value = $this->get_value($values);
    return empty($value) ? t('New Course &bull; ') : NULL;
  }
}

/**
 * Return a string describing the status of new course administrative details.
 *
 * @ingroup views_field_handlers
 */
class catalog_proposals_handler_new_course_admin extends views_handler_field {
  public function render($values) {
    $node = node_load($this->get_value($values));
    if (empty($node->field_crs_master_ref['und'][0]['target_id'])) {
      if (empty($node->field_crs_acscode) && empty($node->field_crs_reimburse_code)) {
        return t('Awaiting ACS and Reimbursement Code');
      }
      elseif (empty($node->field_crs_acscode)) {
        return t('Awaiting ACS Code');
      }
      elseif (empty($node->field_crs_reimburse_code)) {
        return t('Awaiting Reimbursement Code');
      }
    }
  }
}

/**
 * Return the current suggested action for course proposals if applicable.
 *
 * @ingroup views_field_handlers
 */
class catalog_proposals_handler_course_actions extends views_handler_field {
  public function render($values) {
    $node = node_load($this->get_value($values));
    if ($node->field_proposal_processed['und'][0]['value'] != 1) {
      if (empty($node->field_crs_div_review) && (user_access('edit any course_proposal content') || user_access('administer nodes'))) {
          return l(t('Edit'), 'node/' . $node->nid . '/edit');
      }
      elseif (user_access('push proposals')) {
        return l(t('Process'), 'proposals/courses/push/' . $node->nid);
      }
      else {
        return t('Pending');
      }
    }
    else {
      return t('Processed');
    }
  }
}
