<?php

/**
 * @file
 * Views handlers for hfcc_catalog.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function hfcc_catalog_views_data_alter(&$data) {
  $data['node']['fake_program_data'] = array(
    'group' => t('Content'),
    'title' => t('Fake Program Data'),
    'help' => t('Return fake data for programs.'),
    'real field' => 'type',
    'field' => array(
      'handler' => 'hfcc_catalog_handler_field_fake',
      'click sortable' => FALSE,
    ),
  );
}

/**
 * Return degree-specific requirements boilerplate text.
 *
 * @ingroup views_filter_handlers
 */
class hfcc_catalog_handler_field_fake extends views_handler_field {
  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['fake_data'] = array('default' => NULL);
    return $options;
  }

  /**
   * Define user settings form.
   */
  function options_form(&$form, &$form_state) {
    $form['fake_data'] = array(
      '#type' => 'radios',
      '#title' => t('Display options'),
      '#options' => array(
        'degsp' => t('Degree-Specific Requirements Boilerplate'),
        'other' => t('Other Information Header'),
        'xfopt' => t('Transfer Information Header/Boilerplate'),
      ),
      '#default_value' => $this->options['fake_data'],
      '#description' => t('Select which fake data to display.'),
      '#required' => TRUE,
    );
    parent::options_form($form, $form_state);
  }

  /**
  * Render the field.
  */
  public function render($values) {
    $value = $this->get_value($values);
    if ($value == 'program') {
      if ($node = node_load($values->nid)) {
        $degree_type = $node->field_program_type[LANGUAGE_NONE][0]['value'];
        switch ($this->options['fake_data']) {
          case 'degsp':
            if ($items = hfcc_catalog_get_boilerplate_info('DEGSP', $degree_type)) {
              return render($items);
            }
            break;
          case 'other':
            if (!empty($node->field_program_exposure) || !empty($node->field_program_licensure)) {
              // Yes, really. Need this for export to InDesign. Sorry.
              return "<h2>Other Information</h2>\n";
            }
            break;
          case 'xfopt':
            if ($items = hfcc_catalog_get_boilerplate_info('XFOPT', $degree_type)) {
              // Yes, really. Need this for export to InDesign. Sorry.
              return "<h2>Transfer Information</h2>\n" . render($items);
            }
            break;
        }
      }
    }
  }
}
