<?php

/**
 * @file
 * Interface and Controller for EventLocation entities.
 */

class XferCollegeController extends EntityAPIController {

  /**
   * Create a new entity.
   *
   * @param array $values
   *   An array of values to set, keyed by property name.
   * @return
   *   A new instance of the entity type.
   */ 
  public function create(array $values = array()) {
    $values += array(
      'id' => NULL,
      'name' => NULL,
      );
    return parent::create($values);
  }

}