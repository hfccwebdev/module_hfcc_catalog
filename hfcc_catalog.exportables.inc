<?php

/**
 * @file
 * Page callbacks for exportables.
 *
 * @see hfcc_catalog.module
 */

/**
 * Returns an index of catalog courses for use as a services resource.
 *
 * @todo Add support for course availability options.
 */
function _hfcc_catalog_get_catalog_courses($subject = NULL, $number = NULL, $nid = NULL) {

  $cid = 'hfcc_catalog_catalog_courses_index';
  $cid .= !empty($subject) ? '-subject-' . $subject : NULL;
  $cid .= !empty($number) ? '-number-' . $number : NULL;
  $cid .= !empty($nid) ? '-nid-' . $nid : NULL;

  if ($rows = cache_get($cid, 'cache')) {
    return $rows->data;
  }

  $query = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('n.type', 'catalog_course')
    ->condition('n.status', 1)
    ->orderBy('n.title');

  $query->leftjoin('field_data_field_inactive', 'i', "n.nid = i.entity_id AND n.vid = i.revision_id AND i.entity_type = 'node'");
  // This is inconsistent. Fix it below instead.
  // $query->condition('i.field_inactive_value', 0);
  $query->addField('i', 'field_inactive_value', 'inactive');

  $query->leftjoin('field_data_field_crs_subj', 'crs_subj', "n.nid = crs_subj.entity_id AND n.vid = crs_subj.revision_id AND crs_subj.entity_type = 'node'");
  $query->addField('crs_subj', 'field_crs_subj_value', 'subject');
  $query->leftjoin('field_data_field_crs_num', 'crs_num', "n.nid = crs_num.entity_id AND n.vid = crs_num.revision_id AND crs_num.entity_type = 'node'");
  $query->addField('crs_num', 'field_crs_num_value', 'number');

  if (!empty($nid)) {
    $query->condition('n.nid', $nid, '=');
  }
  if (!empty($subject)) {
    $query->condition('crs_subj.field_crs_subj_value', $subject, '=');
  }
  if (!empty($number)) {
    $query->condition('crs_num.field_crs_num_value', $number, '=');
  }

  $query->leftjoin('field_data_field_crs_title', 'crs_title', "n.nid = crs_title.entity_id AND n.vid = crs_title.revision_id AND crs_title.entity_type = 'node'");
  $query->addField('crs_title', 'field_crs_title_value', 'title');

  // We need to send formatted data for divisions and departments, since that's what the original view
  // (for Services Views module) used. For forward compatibility, I'm also including the division and
  // department codes here.

  $query->leftjoin('field_data_field_crs_div', 'crs_div', "n.nid = crs_div.entity_id AND n.vid = crs_div.revision_id AND crs_div.entity_type = 'node'");
  $query->leftjoin('hank_divisions', 'hank_div', "crs_div.field_crs_div_value = hank_div.divisions_id COLLATE utf8_general_ci");
  $query->addField('crs_div', 'field_crs_div_value', 'div_code');
  $query->addField('hank_div', 'div_desc', 'division');
  $query->leftjoin('field_data_field_crs_dept', 'crs_dept', "n.nid = crs_dept.entity_id AND n.vid = crs_dept.revision_id AND crs_dept.entity_type = 'node'");
  $query->leftjoin('hank_depts', 'hank_dept', "crs_dept.field_crs_dept_value = hank_dept.depts_id COLLATE utf8_general_ci");
  $query->addField('crs_dept', 'field_crs_dept_value', 'dept_code');
  $query->addField('hank_dept', 'depts_desc', 'department');

  $query->leftjoin('field_data_field_crs_credit_hours', 'crs_hours', "n.nid = crs_hours.entity_id AND n.vid = crs_hours.revision_id AND crs_hours.entity_type = 'node'");
  $query->addExpression('ROUND(field_crs_credit_hours_value, 2)', 'credit_hours');

  $query->leftjoin('field_data_field_crs_description', 'crs_desc', "n.nid = crs_desc.entity_id AND n.vid = crs_desc.revision_id AND crs_desc.entity_type = 'node'");
  $query->addField('crs_desc', 'field_crs_description_value', 'description');
  $query->addField('crs_desc', 'field_crs_description_format', 'description_format');

  $query->leftjoin('field_data_field_crs_prq', 'crs_prq', "n.nid = crs_prq.entity_id AND n.vid = crs_prq.revision_id AND crs_prq.entity_type = 'node'");
  $query->addField('crs_prq', 'field_crs_prq_value', 'prerequisites');
  $query->leftjoin('field_data_field_crs_crq', 'crs_crq', "n.nid = crs_crq.entity_id AND n.vid = crs_crq.revision_id AND crs_crq.entity_type = 'node'");
  $query->addField('crs_crq', 'field_crs_crq_value', 'corequisites');

  $query->leftjoin('field_data_field_crs_note', 'crs_note', "n.nid = crs_note.entity_id AND n.vid = crs_note.revision_id AND crs_note.entity_type = 'node'");
  $query->addField('crs_note', 'field_crs_note_value', 'note');
  $query->addField('crs_note', 'field_crs_note_format', 'note_format');

  $query->leftjoin('field_data_field_crs_legacy_id', 'crs_legacy_id', "n.nid = crs_legacy_id.entity_id AND n.vid = crs_legacy_id.revision_id AND crs_legacy_id.entity_type = 'node'");
  $query->addField('crs_legacy_id', 'field_crs_legacy_id_value', 'legacy_id');

  $result = $query->execute()->fetchAll();

  $rows = array();
  foreach($result as $item) {
    if (empty($item->inactive)){
      $id = drupal_strtolower($item->subject) . '-' . $item->number;
      $item->title = check_plain($item->title);
      $item->prerequisites = check_plain($item->prerequisites);
      $item->corequisites = check_plain($item->corequisites);
      $item->description = check_markup($item->description, $item->description_format);
      unset($item->description_format);
      $item->note = check_markup($item->note, $item->note_format);
      unset($item->note_format);
      if ($cpcll = _hfcc_catalog_get_cpcll_info($item->nid)) {
        $item->cpcll = $cpcll;
      }
      else {
        $item->cpcll = NULL;
      }
      $rows[] = $item;
    }
  }
  cache_set($cid, $rows, 'cache', CACHE_TEMPORARY);
  return $rows;
}

/**
 * Fetches CPCLL information for the Catalog Course
 */
function _hfcc_catalog_get_cpcll_info($nid) {
  $query = db_select('field_data_field_crs_prior_learning_options', 'cpcll')
    ->fields('cpcll', array('field_crs_prior_learning_options_value'))
    ->condition('cpcll.entity_type', 'node')
    ->condition('cpcll.entity_id', $nid);

    $result = $query->execute()->fetchAll();
    if (!empty($result)) {
      $result = reset($result);
      return !empty($result->field_crs_prior_learning_options_value);
    }
}

/**
 * Returns an index of catalog programs for use as a services resource.
 */
function _hfcc_catalog_programs_index() {

  $query = db_select('node', 'n')
    ->fields('n', array('nid', 'title'))
    ->condition('n.type', 'program')
    ->condition('n.status', 1)
    ->orderBy('n.title');

  $query->leftjoin('field_data_field_program_type', 'type', "n.nid = type.entity_id AND n.vid = type.revision_id AND type.entity_type = 'node'");
  $query->addField('type', 'field_program_type_value', 'degree_type');

  $query->leftjoin('field_data_field_crs_div', 'divn', "n.nid = divn.entity_id AND n.vid = divn.revision_id AND divn.entity_type = 'node'");
  $query->addField('divn', 'field_crs_div_value', 'division');

  $query->leftjoin('field_data_field_crs_dept', 'dept', "n.nid = dept.entity_id AND n.vid = dept.revision_id AND dept.entity_type = 'node'");
  $query->addField('dept', 'field_crs_dept_value', 'department');

  $query->leftjoin('field_data_field_program_office_contact', 'office', "n.nid = office.entity_id AND n.vid = office.revision_id AND office.entity_type = 'node'");
  $query->addField('office', 'field_program_office_contact_target_id', 'office');

  $query->leftjoin('field_data_field_program_legacy_id', 'legacy', "n.nid = legacy.entity_id AND n.vid = legacy.revision_id AND legacy.entity_type = 'node'");
  $query->addField('legacy', 'field_program_legacy_id_value', 'legacy_id');

  $query->leftjoin('field_data_field_program_inactive', 'inactive', "n.nid = inactive.entity_id AND n.vid = inactive.revision_id AND inactive.entity_type = 'node'");
  $query->addField('inactive', 'field_program_inactive_value', 'inactive');

  $result = $query->execute()->fetchAll();

  // Filter inactive programs this way because field data is inconsistent.

  $programs = array();
  foreach($result as $item) {
    if (empty($item->inactive)){
      if ($program_links = _hfcc_catalog_get_program_links($item->nid)) {
        $item->catalog_program_links = $program_links;
      }
      $programs[] = $item;
    }
  }
  return $programs;
}

/**
 * Render program nodes for export as a services resource.
 */
function _hfcc_catalog_programs_export($nid) {
  if ($node = hfcc_catalog_program_node_load($nid)) {
    $output = node_view_multiple(array($node->nid => $node), 'hfcc_catalog_export');
    $program = &$output['nodes'][$nid];

    $export = array(
      'nid' => $node->nid,
      'title' => decode_entities($node->title),
      'content' => render($output),
      'inactive' => (int) !empty($node->field_program_inactive['und'][0]['value']) ? $node->field_program_inactive['und'][0]['value'] : 0,
      'degree_type' => $node->field_program_type[LANGUAGE_NONE][0]['value'],
    );
    return $export;
  }
}
