Henry Ford College Catalog Manager 2.x for Drupal 7.x
=======================================

This module provides custom functionality for the HFC Course Catalog project.

This Drupal module contains custom code exclusive to this project, and while
anyone is welcome to fork and modify this module for their own purposes, they
do so at their own risk, and such usage is not supported by the author(s).

## Installation

HFC Catalog Manager can be installed like any other Drupal module -- place it in
the sites/all/modules/custom directory for your site and enable it.

## Usage

This module assumes the creation of three node types, *catalog_boilerplate*,
*catalog_course* and *program*.

Currently, *catalog_course* nodes will only be used to look up courses and
create links to them, so all they need is a nid and title.

This module provides custom field types that are used to generate lists
of required courses and course sequences for *program* nodes.

This module also embeds values for *field_bp_text* and *field_bp_webpage*
from the *catalog_boilerplate* content type into the *program* node, For
this to work, *catalog_boilerplate* nodes also need *field_degree_type*
and a field called field_bp_section, whose values are defined in
hfcc_catalog_boilerplate_sections().
