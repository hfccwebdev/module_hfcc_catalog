<?php

/**
 * @file
 * Includes temporary routines to recover legacy program_sequence data
 * for HFC WebAdmin Issue 2752 and anything else that needs it.
 */

/**
 * Page callback for legacy sequence info.
 */
function hfcc_catalog_issue2752($nid) {

  $result = db_select('hfcc_program_sequence_backup', 'b')
    ->fields('b')
    ->condition('b.nid', $nid)
    ->orderBy('b.delta')
    ->execute()
    ->fetchAll();
    ;

  if (!empty($result)) {
    // dpm($result, 'result');

    drupal_set_title(t('Saved Sequence(s) for @title', array('@title' => $result[0]->title)));

    $output = array();

    foreach ($result as $sequence) {

        $data = unserialize($sequence->field_program_sequence_data);
        // dpm($data, 'sequence ' . $sequence->delta);
        $header = array();
        $rows = array();
        foreach (array_keys($data) as $term) {
          $terms = hfcc_catalog_program_sequence_terms($term + 1);
          $header[$term] = !empty($terms[$data[$term]['label']]) ? $terms[$data[$term]['label']] : $data[$term]['label'];
          foreach (array_keys($data[$term]['courses']) as $row) {
            // We need to fill across each new row or else the table columns won't line up.
            if (empty($rows[$row])) {
              for ($i=0; $i < count($data); $i++) {
                $rows[$row][$i] = NULL;
              }
            }
            // Now build the data. Use the course if provided, otherwise display the note.
            $item = $data[$term]['courses'][$row];
            if (!empty($item['course_id'])) {
              $markup = hfcc_catalog_program_course_link($item['course_id'], 'program_sequence_formatter');
            }
            elseif (!empty($item['note'])) {
              $markup = check_plain($item['note']);
            }
            else {
              $markup = t('Error!');
            }
            $rows[$row][$term] = $markup;
          }
        }
        if (!empty($rows)) {
          $output[] = array(
            '#prefix' => '<h3 class="title">',
            '#markup' => t('Sequence @delta', array('@delta' => $sequence->delta)),
            '#suffix' => '</h3>',
          );
          $output[] = array(
            '#theme' => 'table',
            '#header' => $header,
            '#rows' => $rows,
          );
        }

    }
    return $output;
  }
  else {
    drupal_not_found();
    drupal_exit();
  }

}
