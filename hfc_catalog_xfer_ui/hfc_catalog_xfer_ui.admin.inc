<?php

/**
 * @file
 * Contains administrative forms for managing transfer schools.
 *
 * @see hfc_catalog_xfer_ui.module
 */

/**
 * Page callback for transfer colleges.
 */
function hfc_catalog_xfer_ui_colleges_page() {
  $result = db_query("SELECT * FROM {hfcc_catalog_xfer_colleges} ORDER BY name")->fetchAll();
  $rows = array();
  foreach ($result as $item) {
    $actions = array(
      l(t('edit'), 'admin/content/xfer-colleges/' . $item->id . '/edit'),
      // l(t('delete'), 'admin/content/xfer-colleges/' . $item->id . '/delete'),
    );
    $rows[] = array(
      check_plain($item->name),
      implode(' ', $actions),
    );
  }
 
  return array(
    '#theme' => 'table',
    '#header' => array(
      t('Name'),
      t('Actions'),
    ),
    '#rows' => $rows,
  );
}

/**
 * Transfer college edit form.
 */
function hfc_catalog_xfer_ui_colleges_form($form, &$form_state, $entity) {
  // Set the page title.
  if (empty($entity->is_new)) {
    $form_title = t('Editing @title', array('@title' => $entity->name));
  }
  else {
    $form_title = t('Add new transfer college');
  }
  drupal_set_title($form_title);

  $form = array();

  // Include the entity as a value. This will not be passed to the browser,
  // but can be used by the submit handler.
  $form['entity'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('College Name'),
    '#default_value' => trim($entity->name),
    '#description' => t('The transfer college name.'),
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  field_attach_form('xfer_college', $entity, $form, $form_state);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );

  return $form;
}

/**
 * Validate the entity edit form.
 */
function hfc_catalog_xfer_ui_colleges_form_validate($form, &$form_state) {
    $values = (object) $form_state['values'];
    field_attach_form_validate('xfer_college', $values, $form, $form_state);
}

/**
 * Process Transfer College form submissions.
 *
 * @see hfcc_catalog_xfer_colleges_form()
 */
function hfc_catalog_xfer_ui_colleges_form_submit($form, &$form_state) {

  $entity = $form_state['values']['entity'];
  $entity->name = $form_state['values']['name'];
  field_attach_submit('xfer_college', $entity, $form, $form_state);
  entity_save('xfer_college', $entity);
  $form_state['redirect'] = 'admin/content/xfer-colleges';
}

/**
 * Add new hfcc_catalog_xfer_colleges entry.
 */
function hfc_catalog_xfer_ui_colleges_add() {
  $entity = entity_create('xfer_college', array());
  $entity->type = 0;
  return drupal_get_form('hfc_catalog_xfer_ui_colleges_form', $entity);
}
