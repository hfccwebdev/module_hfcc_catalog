<?php

/**
 * @file
 * This module contains the code for HFC's proposal-based curriculum management system.
 *
 * @ingroup hfcc_modules
 */

/**
 * Implements hook_init().
 */
function catalog_proposals_init() {
  module_load_include('inc', 'catalog_proposals', 'catalog_proposals.courses');
  module_load_include('inc', 'catalog_proposals', 'catalog_proposals.programs');
  module_load_include('inc', 'catalog_proposals', 'catalog_proposals.views');
}

/**
 * Implements hook_permission().
 */
function catalog_proposals_permission() {
  return array(
    'bypass workflow' => array(
      'title' => t('Bypass proposal workflow'),
      'description' => t('Edit fields on proposals that have already been partially approved.'),
    ),
    'edit div fields' => array(
      'title' => t('Edit division-level fields'),
    ),
    'edit si fields' => array(
      'title' => t('Edit S&I fields'),
    ),
    'edit reg fields' => array(
      'title' => t('Edit Registrar fields'),
    ),
    'edit curric fields' => array(
      'title' => t('Edit Curriculum Management Fields'),
    ),
    'push proposals' => array(
      'title' => t('Push proposed changes to masters'),
    ),
  );
}

/*
 * Implements hook_menu().
 */
function catalog_proposals_menu() {
  return array(
    'proposals/courses/push/%node' => array(
      'title' => 'Sync Approved Course Proposal',
      'page callback' => 'catalog_proposals_course_push',
      'page arguments' => array(3),
      'access arguments' => array('push proposals'),
      'weight' => 20,
      'type' => MENU_CALLBACK,
      'file' => 'catalog_proposals.courses.inc',
    ),
  );
}

/**
 * Implements hook_menu_alter().
 */
function catalog_proposals_menu_alter(&$items) {
  $items['node/add/course-proposal']['page callback'] = 'drupal_get_form';
  $items['node/add/course-proposal']['page arguments'] = array('catalog_proposals_course_add');
  $items['node/add/program-proposal']['page callback'] = 'drupal_get_form';
  $items['node/add/program-proposal']['page arguments'] = array('catalog_proposals_program_add');
  return $items;
}

/**
 * Implements hook_views_api().
 */
function catalog_proposals_views_api() {
  return array(
    'api' => 3,
  );
}


function catalog_proposals_form_alter(&$form, &$form_state, $form_id) {

      unset($form['field_crs_xref']['und']['#options']['_none']);
      unset($form['field_crs_grading']['und']['#options']['_none']);
      unset($form['field_crs_gened_outcomes']['und']['#options']['_none']);
      unset($form['field_crs_macrao']['und']['#options']['_none']);
      unset($form['field_crs_mta_submitted']['und']['#options']['_none']);
      unset($form['field_crs_mta']['und']['#options']['_none']);
      unset($form['field_crs_reimburse_code']['und']['#options']['_none']);
      unset($form['field_crs_remedial']['und']['#options']['_none']);
      unset($form['field_crs_selfpaced']['und']['#options']['_none']);
      unset($form['field_crs_special_charges']['und']['#options']['_none']);
      unset($form['field_crs_fee_schedule']['und']['#options']['_none']);
      unset($form['field_crs_student_audit']['und']['#options']['_none']);
      unset($form['field_crs_transfer']['und']['#options']['_none']);
      unset($form['field_crs_waitlist']['und']['#options']['_none']);
      unset($form['field_crs_library_support']['und']['#options']['_none']);
      unset($form['field_crs_lab_support']['und']['#options']['_none']);

  switch ($form_id) {
    case 'course_proposal_node_form':
      $form['revision_information']['#group'] = NULL;
      $form['revision_information']['#weight'] = 99;
      $form['revision_information']['#collapsible'] = FALSE;
      $form['revision_information']['log']['#required'] = TRUE;
      hide($form['field_crs_master_ref']);
      hide($form['field_proposal_processed']);
      if ($form['field_crs_mta']['und']['#default_value'] == "yes") {
        if ($form['field_crs_mta_submitted']['und']['#default_value'] == "no") {
          unset($form['field_crs_mta_submitted']);
        }
        elseif ($form['field_crs_mta_submitted']['und']['#default_value'] == "yes") {
          $form['field_crs_mta_submitted']['#disabled'] = TRUE;
        }
      }
      if (!empty($form['field_crs_master_ref']['und']['#default_value'])) {
        $form['field_crs_subj']['#disabled'] = TRUE;
        $form['field_crs_num']['#disabled'] = TRUE;
        unset($form['field_hank_courses_id']);
        unset($form['field_crs_short_title']);
        unset($form['field_crs_fac_qual']);
        unset($form['field_crs_gened_outcomes']);
        field_group_hide_field_groups($form, array('group_crs_new'));
      }
      else {
        hide($form['field_crs_macrao']);
        unset($form['field_crs_deactivation_date']);
        $form['field_crs_reg_updated']['und'][0]['#description'] = t('Please be sure to provide the HANK Course ID and Short Title before noting this new course as entered in HANK.');
      }

      if(!user_access('edit curric fields')) {
        $form['field_crs_subj']['#disabled'] = TRUE;
        $form['field_crs_num']['#disabled'] = TRUE;
        $form['field_crs_gened_outcomes']['#disabled'] = TRUE;
        $form['field_crs_ac_approval']['#disabled'] = TRUE;
        $form['field_crs_cc_approval']['#disabled'] = TRUE;
      }
      if(!user_access('edit div fields')) {
        $form['field_crs_macrao']['#disabled'] = TRUE;
        $form['field_crs_mta']['#disabled'] = TRUE;
        $form['field_crs_div_approval']['#disabled'] = TRUE;
        $form['field_crs_div_review']['#disabled'] = TRUE;
      }
      if(!user_access('edit reg fields')) {
        $form['field_hank_courses_id']['#disabled'] = TRUE;
        $form['field_crs_short_title']['#disabled'] = TRUE;
        $form['field_crs_reg_updated']['#disabled'] = TRUE;
      }
      if(!user_access('edit si fields')) {
        $form['field_crs_acscode']['#disabled'] = TRUE;
        $form['field_crs_reimburse_code']['#disabled'] = TRUE;
      }

      if(!empty($form['field_crs_ac_approval']['und'][0]['#default_value']['value'])) {
        $form['field_crs_mta_submitted']['#disabled'] = TRUE;
        if(!user_access('bypass workflow')) {
          // Basic Info
          $form['field_crs_master_ref']['#disabled'] = TRUE;
          $form['field_crs_div']['#disabled'] = TRUE;
          $form['field_crs_dept']['#disabled'] = TRUE;
          $form['field_crs_subj']['#disabled'] = TRUE;
          $form['field_crs_num']['#disabled'] = TRUE;
          $form['field_crs_title']['#disabled'] = TRUE;
          $form['field_crs_xref']['#disabled'] = TRUE;
          $form['field_crs_xref_id']['#disabled'] = TRUE;
          $form['field_crs_credit_hours']['#disabled'] = TRUE;
          $form['field_crs_instructor_hours']['#disabled'] = TRUE;
          $form['field_crs_student_hours']['#disabled'] = TRUE;
          $form['field_crs_grading']['#disabled'] = TRUE;
          $form['field_crs_prq']['#disabled'] = TRUE;
          $form['field_crs_crq']['#disabled'] = TRUE;
          $form['field_crs_description']['#disabled'] = TRUE;

          // Goals, Topics and Objectives
          $form['field_crs_goals']['#disabled'] = TRUE;
          $form['field_crs_topics']['#disabled'] = TRUE;
          $form['field_crs_lrn_objectives']['#disabled'] = TRUE;
          $form['field_crs_dtl_objectives']['#disabled'] = TRUE;
          $form['field_crs_general_info']['#disabled'] = TRUE;

          // Assessment and Requirements
          $form['field_crs_assessment']['#disabled'] = TRUE;
          $form['field_crs_requirements']['#disabled'] = TRUE;
          $form['field_crs_fac_qual']['#disabled'] = TRUE;
          $form['field_crs_texts']['#disabled'] = TRUE;

          // Credit for Prior College-Level Learning
          $form['field_crs_prior_learning_options']['#disabled'] = TRUE;
          $form['field_crs_cpcll_fee']['#disabled'] = TRUE;
          $form['field_crs_prior_dept_exam_score']['#disabled'] = TRUE;
          $form['field_crs_prior_dept_exam']['#disabled'] = TRUE;
          $form['field_crs_prior_other_exam_score']['#disabled'] = TRUE;
          $form['field_crs_prior_other_exam']['#disabled'] = TRUE;
          $form['field_crs_prior_skilled_demo']['#disabled'] = TRUE;
          $form['field_crs_prior_portfolio']['#disabled'] = TRUE;
          $form['field_crs_prior_license']['#disabled'] = TRUE;
          $form['field_crs_prior_licensure']['#disabled'] = TRUE;
          $form['field_crs_prior_interview']['#disabled'] = TRUE;
          $form['field_crs_prior_eval']['#disabled'] = TRUE;

          // New Course
          $form['field_crs_remedial']['#disabled'] = TRUE;
          $form['field_crs_selfpaced']['#disabled'] = TRUE;
          $form['field_crs_special_charges']['#disabled'] = TRUE;
          $form['field_crs_course_fee']['#disabled'] = TRUE;
          $form['field_crs_lab_fee']['#disabled'] = TRUE;
          $form['field_crs_misc_dept_fee']['#disabled'] = TRUE;
          $form['field_crs_fee_schedule']['#disabled'] = TRUE;
          $form['field_crs_fee_desc']['#disabled'] = TRUE;
          $form['field_crs_student_audit']['#disabled'] = TRUE;
          $form['field_crs_transfer']['#disabled'] = TRUE;
          $form['field_program_xfer_colleges']['#disabled'] = TRUE;
          $form['field_crs_transfer_notes']['#disabled'] = TRUE;
          $form['field_crs_capacity']['#disabled'] = TRUE;
          $form['field_crs_waitlist']['#disabled'] = TRUE;
          $form['field_crs_replacing']['#disabled'] = TRUE;
          $form['field_crs_course_rationale']['#disabled'] = TRUE;
          $form['field_crs_first_term_enroll']['#disabled'] = TRUE;
          $form['field_crs_first_term_sections']['#disabled'] = TRUE;
          $form['field_crs_staffing']['#disabled'] = TRUE;
          $form['field_crs_equipment_needs']['#disabled'] = TRUE;
          $form['field_crs_library_support']['#disabled'] = TRUE;
          $form['field_crs_lab_support']['#disabled'] = TRUE;

          // Effective and Approval Dates
          $form['field_effective_term']['#disabled'] = TRUE;
          $form['field_crs_deactivation_date']['#disabled'] = TRUE;
          $form['field_crs_div_approval']['#disabled'] = TRUE;

          drupal_set_message(t('This course proposal has been approved by AALC and can no longer be edited.'));
        }
      }

      if (empty($form['field_crs_div_approval']['und'][0]['#default_value']['value'])) {
        hide($form['field_crs_ac_approval']);
      }
      if (empty($form['field_crs_ac_approval']['und'][0]['#default_value']['value'])) {
        unset($form['field_crs_cc_approval']);
      }
      if (empty($form['field_crs_master_ref']['und']['#default_value']) && ((empty($form['field_crs_acscode']['und'][0]['value']['#default_value'])) || (empty($form['field_crs_reimburse_code']['und']['#default_value'])))) {
        $form['field_crs_cc_approval']['#disabled'] = TRUE;
        $form['field_crs_cc_approval']['und'][0]['#description'] = t('This new course must be approved by AALC and have both an ACS and Reimbursement Code before it can be considered for approval by the Curriculum Committee.');
      }
      if (empty($form['field_crs_cc_approval']['und'][0]['#default_value']['value'])) {
        unset($form['field_crs_reg_updated']);
      }
      if (empty($form['field_crs_reg_updated']['und'][0]['#default_value']['value'])) {
        unset($form['field_crs_div_review']);
      }
      if ($form['field_proposal_processed']['und']['#default_value'] == 1) {
        foreach (element_children($form) as $key) {
          $form[$key]['#disabled'] = TRUE;
        }
        drupal_set_message(t('This proposal has been fully approved and processed and can no longer be edited.'));
      }

      break;

    case 'program_proposal_node_form':
      hide($form['field_program_master_ref']);
      hide($form['field_proposal_processed']);
      $form['revision_information']['log']['#required'] = TRUE;

      if (!empty($form['field_program_master_ref']['und']['#default_value'])) {
        // New Program Fields
        unset($form['field_program_est_tuit']);
        unset($form['field_program_div_dept_code']);
        unset($form['field_program_additional_costs']);
        unset($form['field_program_costs_desc']);
        unset($form['field_program_occupational_code']);
        unset($form['field_program_year']);
        unset($form['field_program_first_accepted']);
        unset($form['field_program_eligible_graduate']);
        unset($form['field_program_finaid_eligible']);
        unset($form['field_program_testing_exempt']);
        unset($form['field_program_cert_half_new']);
        unset($form['field_program_cert_faculty_qual']);
        unset($form['field_program_academic_control']);
        unset($form['field_program_cert_license_req']);
        unset($form['field_program_cert_org']);
        unset($form['field_program_cert_vendor_spec']);
        unset($form['field_program_cert_vendor']);
        unset($form['#fieldgroups']['group_program_new']);

        // Dates
        unset($form['field_program_hlc_request']);
        unset($form['field_program_hlc_approval']);
        unset($form['field_program_sbe_request']);
        unset($form['field_program_sbe']);
      }
      else {
        unset($form['field_program_additional_costs']['und']['#options']['_none']);
        unset($form['field_program_occupational_code']['und']['#options']['_none']);
        unset($form['field_program_finaid_eligible']['und']['#options']['_none']);
        unset($form['field_program_testing_exempt']['und']['#options']['_none']);
        unset($form['field_program_cert_half_new']['und']['#options']['_none']);
        unset($form['field_program_cert_license_req']['und']['#options']['_none']);
        unset($form['field_program_cert_vendor_spec']['und']['#options']['_none']);
      }

      break;

  }
}
